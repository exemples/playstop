Router.configure({
    layoutTemplate: 'layout',
    loadingTemplate: 'loading',
    notFoundTemplate: 'notFound'
});

Meteor.startup(function () {
    loadLetters();
});

function loadLetters(){

    if ( Letters.find({}).count() == 0 ){
        insertLettersDB();
    } else if ( Letters.find({}).count() != 25 ){
        removeLettersAll();
        insertLettersDB();
    }
}

function removeLettersAll(){
    Letters.find({}).forEach(
        function(i) {
            Letters.remove(i);
        }
    )
}

function insertLettersDB(){
    var count = 1;
    takeAllLetters().forEach(
        function(i) {
            Letters.insert({key:count,value:i})
            count += 1;
        }
    )
}

function takeAllLetters(){
    var letters = [
        'A','B','C','D','E',
        'F','G','H','I','J',
        'L','M','N','O','P',
        'Q','R','S','T','U',
        'V','X','Z','Y','K'
    ];
    return letters;
}