if(Meteor.isClient){
    Meteor.startup(function () {
        Session.set("showLoadingIndicator", true);
        var userLanguage = window.navigator.userLanguage || window.navigator.language;
        var language = 'en-US';
        if(userLanguage.toLowerCase()=="pt-br"){
            language = 'pt-BR';
        }
        TAPi18n.setLanguage(language)
            .done(function () {
                Session.set("showLoadingIndicator", false);
            })
            .fail(function (error_message) {
                console.log(error_message);
            });
    });
}