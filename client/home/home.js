var letter = new ReactiveVar("");
Template.home.events({
    'click #start': function (e) {
        var id = getRandoInt(1,25);
        letter.set(Letters.findOne({key:id}).value);
    }
});


Template.home.helpers({
    'letter':function(){
        return letter.get();
    },
    isShowResult:function(){
        return letter.get().length > 0;
    }
});


function getRandoInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

